# Copilot Exercise

### intro
we're building an MVP not a final product
this is used for copilot learning
your workflow should primarily be prompting copilot and then fixing the code it gives you
this may take you 1-2 hours, it may take the full day

### Instructions
1. set up
    - clone the repo https://gitlab.galvanize.com/cohorts/consumer/sjp/forkable-projects/copilot-exercise/-/tree/start?ref_type=heads
    - build .env file, example in `.env.sample`
    - docker compose build
    - docker compose up
    - go to the API's docker terminal and `python -m seed`
    - got to localhost:8000 and sign up a new user
2. use copilot to create an "update store item inventory" fetch
    - (small warm up)
    - see `ghi/src/components/ShopApparelRow.jsx`
    - it should update the DB with the new quantity then show the updated shop's data on the page
3. use copilot to create a "add apparel to shop" page, fetch, redirect
    - you may need to create a new file to make this work
    - build out the AddToShopPage used in main.jsx
    - it will need to get all the possible apparels
    - show them and a quantity input in a form
    - have a submit function that creates a new shop_apparels row
    - redirect to the shop page on success
4. stretch goal/bonus work
    - figure out solution for assigning shop id on sign up
    - (as of now it's hardcoded)