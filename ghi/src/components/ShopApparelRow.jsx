import React, { useState } from 'react';

import useAuthService from '../authService';

const ShopApparelRow = ({ item, getShopData }) => {
    const [isEditing, setIsEditing] = useState(false);
    const [quantity, setQuantity] = useState(item.quantity);
    const { user } = useAuthService();

    const handleChangeQuantity = (e) => {
        setQuantity(e.target.value);
    };

    const handleUpdateShopApparel = async () => {
        // use copilot to come up with the code that belongs here
    }

    const handleEditClick = () => {
        setIsEditing(true);
    };

    const handleDeleteClick = async () => {
        const url = `${import.meta.env.VITE_API_HOST}/shop_apparels/${item.id}/${user.shop_id}`;
        const response = await fetch(url, {
            method: 'DELETE',
            credentials: 'include',
        });
        if (response.ok) {
            getShopData();
        } else {
            console.error('Failed to delete shop apparel');
        }
    };

    return (
        <tr>
            <td>{item.item}</td>
            <td>{item.description}</td>
            <td>{item.size}</td>
            <td>{item.price}</td>
            <td>{!isEditing ? item.quantity : (
                <input
                    type="number"
                    value={quantity}
                    onChange={handleChangeQuantity}
                    style={{ width: '65px' }}
                />
            )}</td>
            <td>{!isEditing ? <span /> : (
                <button onClick={handleUpdateShopApparel}>Save</button>
            )}</td>
            <td>{!isEditing ? <button onClick={handleEditClick}>Edit</button> : <span />}</td>
            <td><button onClick={handleDeleteClick}>Delete</button></td>
        </tr>
    );
};

export default ShopApparelRow;
