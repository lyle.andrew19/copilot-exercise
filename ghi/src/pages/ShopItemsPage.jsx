import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import useAuthService from "../authService";
import NavBar from "../components/NavBar";
import ShopApparelRow from "../components/ShopApparelRow";


function ShopItemsPage() {
  const [shop, setShop] = useState(null);
  const { user, isLoaded } = useAuthService();
  const navigate = useNavigate();

  const getShopData = async () => {
    const url = `${import.meta.env.VITE_API_HOST}/shops/${user.shop_id}`;
    const res = await fetch(url, {
        credentials: "include",
        headers: {
            "Content-Type": "application/json",
        },
    });
    if (res.ok) {
        const newShop = await res.json();
        setShop(newShop);
    }
  };

  useEffect(() => {
    if (user?.shop_id) {
        getShopData();
    }
    if (!user && isLoaded) {
        navigate("/");
    }
  }, [user, isLoaded]);

  return (
    <div>
        <NavBar />
        {shop ? (
            <div id="page-container">
                <div id="table-title">
                    <div>{shop.name}</div>
                    <div><button onClick={() => navigate(`/add-to-shop-apparel`)}>Add Apparel</button></div>
                </div>
                <table id="shop-items-table">
                    <thead>
                        <tr>
                            <th>Item</th>
                            <th>Description</th>
                            <th>Size</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th style={{ width: "75px" }}>{/* save */}</th>
                            <th>{/* edit */}</th>
                            <th>{/* delete */}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {shop.apparels?.map((item) => (
                            <ShopApparelRow key={item.id} item={item} getShopData={getShopData} />
                        ))}
                    </tbody>
                </table>
            </div>
        ) : null}
    </div>
  );
}

export default ShopItemsPage;
