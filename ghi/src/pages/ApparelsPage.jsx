import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

import useAuthService from "../authService";
import NavBar from "../components/NavBar";
import ApparelRow from "../components/ApparelRow";


function ApparelsPage() {
  const [apparels, setApparels] = useState([]);
  const { user, isLoaded } = useAuthService();
  const navigate = useNavigate();

  const getApparels = async () => {
      const url = `${import.meta.env.VITE_API_HOST}/apparels`;
      const res = await fetch(url, {
          credentials: "include",
          headers: {
              "Content-Type": "application/json",
          },
      });
      if (res.ok) {
          const newApparels = await res.json();
          setApparels(newApparels);
      }
  };

  useEffect(() => {
    if (!user && isLoaded) {
      navigate("/");
    } else {
        getApparels();
    }
  }, [user]);

  return (
    <>
      <NavBar />
      <div id="page-container">
        <div id="table-title">
          <div>All Apparel</div>
          <div><button onClick={() => navigate(`/add-new-apparel`)}>Add New Apparel</button></div>
        </div>
        <table id="shop-items-table">
          <thead>
              <tr>
                  <th>Item</th>
                  <th>Description</th>
                  <th>Size</th>
                  <th>Price</th>
                  <th>{/* edit */}</th>
                  <th>{/* delete */}</th>
              </tr>
          </thead>
          <tbody>
            {apparels.map((item) => (
              <ApparelRow key={item.id} item={item} getApparels={getApparels} />
            ))}
          </tbody>
        </table>
      </div>
    </>
  );
}

export default ApparelsPage;
