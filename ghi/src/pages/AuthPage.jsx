import { useState } from "react";
import { useNavigate } from "react-router-dom";

import useAuthService from "../authService";
import NavBar from "../components/NavBar";


function AuthPage() {
  const [signInData, setSignInData] = useState({});
  const [signUpData, setSignUpData] = useState({});
  const { signin, signup } = useAuthService();
  const navigate = useNavigate();

  const handleSignInFormChange = (e) => {
    const { name, value } = e.target;
    setSignInData({ ...signInData, [name]: value });
  };

  const handleSignIn = async (e) => {
    e.preventDefault();
    const success = await signin(signInData);
    if (success) {
        setSignInData({});
        navigate("/shop-items");
    }
  };

  const handleSignUpFormChange = (e) => {
    const { name, value } = e.target;
    setSignUpData({ ...signUpData, [name]: value });
  };

  const handleSignUp = async (e) => {
    e.preventDefault();
    const success = await signup(signUpData);
    if (success) {
        setSignUpData({});
        navigate("/shop-items");
    }
  };

  return (
    <>
      <NavBar />
      <div id="side-by-side-container">
        <div className="form-container">
              <form id="signup-form" onSubmit={handleSignUp}>
                  <input type="text" name="first_name" placeholder="first_name" onChange={handleSignUpFormChange} />
                  <input type="text" name="last_name" placeholder="last_name" onChange={handleSignUpFormChange} />
                  <input type="text" name="email" placeholder="email" onChange={handleSignUpFormChange} />
                  <input type="password" name="password" placeholder="password" onChange={handleSignUpFormChange} />
                  <input type="submit" value="sign up" />
              </form>
          </div>
        <div className="form-container">
              <form id="signin-form" onSubmit={handleSignIn}>
                  <input type="text" name="email" placeholder="email" onChange={handleSignInFormChange} />
                  <input type="password" name="password" placeholder="password" onChange={handleSignInFormChange} />
                  <input type="submit" value="sign in" />
              </form>
          </div>
      </div>
    </>
  );
}

export default AuthPage;
