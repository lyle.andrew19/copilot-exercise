from pydantic import BaseModel

from data_access.connection import pool


class Apparel(BaseModel):
    id: int
    item: str
    description: str
    size: str
    price: int
    quantity: int

class ShopIn(BaseModel):
    name: str
    street: str
    city: str
    state: str
    zip: str

class ShopOut(ShopIn):
    id: int
    apparels: list[Apparel] = []

def sanitize_shops(rows):
    tracker = {}
    for row in rows:
        tracker[row[0]] = tracker.get(row[0], ShopOut(id=row[0], name=row[1], street=row[2], city=row[3], state=row[4], zip=row[5], apparels=[]))
        if row[6] is not None:
            tracker[row[0]].apparels.append(Apparel(id=row[6], item=row[7], description=row[8], size=row[9], price=row[10], quantity=row[11]))
    return list(tracker.values())

def create_shop(shop):
    with pool.connection() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                    INSERT INTO shops (name, street, city, state, zip)
                    VALUES (%s, %s, %s, %s, %s)
                    RETURNING *;
                """,
                [shop.name, shop.street, shop.city, shop.state, shop.zip],
            )
            row = cur.fetchone()
            return sanitize_shops([row])[0]

def read_shops():
    with pool.connection() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                    SELECT
                        s.id,
                        s.name,
                        s.street,
                        s.city,
                        s.state,
                        s.zip,
                        a.id,
                        a.item,
                        a.description,
                        a.size,
                        a.price,
                        sa.quantity
                    FROM shops AS s
                    LEFT OUTER JOIN shop_apparels AS sa ON s.id = sa.shop_id
                    LEFT OUTER JOIN apparels AS a ON sa.apparel_id = a.id
                """
            )
            results = cur.fetchall()
            print('results:', results)
            shops = sanitize_shops(results)
            return shops

def read_shop_by_id(shop_id):
    with pool.connection() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                    SELECT
                        s.id,
                        s.name,
                        s.street,
                        s.city,
                        s.state,
                        s.zip,
                        a.id,
                        a.item,
                        a.description,
                        a.size,
                        a.price,
                        sa.quantity
                    FROM shops AS s
                    LEFT OUTER JOIN shop_apparels AS sa ON s.id = sa.shop_id
                    LEFT OUTER JOIN apparels AS a ON sa.apparel_id = a.id
                    WHERE s.id = %s
                """,
                [shop_id],
            )
            results = cur.fetchall()
            return sanitize_shops(results)[0]

def update_shop(shop_id, shop):
    with pool.connection() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                    UPDATE shops
                    SET name = %s, street = %s, city = %s, state = %s, zip = %s
                    WHERE id = %s
                    RETURNING *;
                """,
                [shop.name, shop.street, shop.city, shop.state, shop.zip, shop_id],
            )
            return sanitize_shops([cur.fetchone()])[0]

def delete_shop(shop_id):
    with pool.connection() as conn:
        with conn.cursor() as cur:
            cur.execute(
                """
                    DELETE FROM shops
                    WHERE id = %s
                """,
                [shop_id],
            )
            return True
