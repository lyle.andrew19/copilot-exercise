from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os

from routers.shop import router as shop_router
from routers.auth import router as auth_router
from routers.apparel import router as apparel_router
from routers.shop_apparel import router as shop_apparel_router


app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=[
        os.environ.get("CORS_HOST", "http://localhost:5173")
    ],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(shop_router)
app.include_router(auth_router)
app.include_router(apparel_router)
app.include_router(shop_apparel_router)
