fastapi[all]==0.78.0
uvicorn[standard]==0.17.6
pytest
psycopg[binary,pool]==3.1.15
python-jose==3.3.0
passlib==1.7.4
