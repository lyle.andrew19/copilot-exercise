query = """
    INSERT INTO shops (name, street, city, state, zip)
    VALUES ('Fashion Avenue', '456 Fashion Street', 'Seattle', 'WA', '98101'),
           ('Style Central', '789 Style Avenue', 'Auburn', 'WA', '98000'),
           ('Trendy Threads', '321 Trendy Road', 'Seattle', 'WA', '98101'),
           ('Chic Boutique', '987 Chic Boulevard', 'Seattle', 'WA', '98101'),
           ('Fashion Forward', '654 Fashion Lane', 'Kent', 'WA', '98200');
"""
