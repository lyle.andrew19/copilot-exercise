query = """
    INSERT INTO employees (first_name, last_name, email, hashed_password, shop_id)
    VALUES ('John', 'Doe', 'john.doe@example.com', 'hashed_password_1', 1),
           ('Jane', 'Smith', 'jane.smith@example.com', 'hashed_password_2', 1),
           ('Mike', 'Johnson', 'mike.johnson@example.com', 'hashed_password_3', 2),
           ('Emily', 'Brown', 'emily.brown@example.com', 'hashed_password_4', 2)
"""

