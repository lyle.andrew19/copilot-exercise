query = """
    INSERT INTO apparels (item, description, size, price)
    VALUES ('T-Shirt', 'Cotton crew neck t-shirt', 'M', 19.99),
           ('Jeans', 'Slim-fit denim jeans', '32x32', 49.99),
           ('Dress', 'Floral print summer dress', 'S', 39.99),
           ('Sneakers', 'Canvas sneakers', '9', 29.99),
           ('Jacket', 'Leather biker jacket', 'L', 79.99);
"""
